import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos:DestinoViaje[];
  constructor() { 
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string,url:string):boolean{
    if(nombre && url && nombre !='' && url !=''){
      this.destinos.push(new DestinoViaje(nombre,url))
    }else{
      alert('Debes completar todos los campos del fomulario');
    }
    return false;
  }
}
